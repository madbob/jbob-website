<div class="filterable-panel" data-endpoint="/samples/filterable.php">
    <input type="search" name="term" class="form-control" placeholder="Search">

    <div class="filterable-block">
        <div class="card mt-3">
            <div class="card-body">
                <?php

                $term = strip_tags($_GET['term'] ?? '');

                if (empty($term) == false) {
                    echo 'You search ' . $term;
                }
                else {
                    echo 'Type something';
                }

                ?>
            </div>
        </div>
    </div>
</div>
