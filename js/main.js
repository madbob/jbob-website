import jBob from '/js/jbob.js';

$(document).ready(function() {
    let j = new jBob();
    j.init({
        dynamicFunctions: {
            sampleFunction: () => {
                alert('submitted!');
            }
        }
    });

    const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]');
    const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl));
});
