<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>jBob: Opinionated utilities for jQuery and Bootstrap</title>
    <meta name="description" content="Opinionated utilities for jQuery and Bootstrap">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.1/font/bootstrap-icons.css">
    <link href="/css/mine.css" rel="stylesheet">

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/masonry-layout@4.2.2/dist/masonry.pkgd.min.js" integrity="sha384-GNFwBvfVxBkLMJpYMOABq3c+d3KnQxudP/mGPkzpZSTYykLBNsZEnG2D9G/X/+7D" crossorigin="anonymous" async></script>
    <script type="module" src="/js/main.js"></script>

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="jBob">
    <meta name="twitter:description" content="Opinionated utilities for jQuery and Bootstrap">
    <meta name="twitter:creator" content="@madbob">
    <meta name="twitter:image" content="https://jbob.madbob.org/images/fb.png">

    <meta property="og:title" content="jBob" />
    <meta property="og:site_name" content="Opinionated utilities for jQuery and Bootstrap" />
    <meta property="og:url" content="https://jbob.madbob.org/" />
    <meta property="og:image" content="https://jbob.madbob.org/images/fb.png" />
    <meta property="og:type" content="website" />
    <meta property="og:locale" content="it" />
</head>

<body>
    <div>
        <section class="header-wrapper text-center">
            <div>
                <h1 class="display-1 fw-bold">jBob</h1>
                <h2>Opinionated utilities<br>for jQuery and Bootstrap</h2>
            </div>
        </section>

        <!-- FAQ -->

        <section class="p-0 pt-4">
            <div class="container">
                <div class="row pb-3 justify-content-center">
                    <div class="col-md-6 position-relative">
                        <p class="text-center display-3">
                            npm install jbob
                        </p>

                        <div class="d-none d-md-flex position-absolute inspector-invite top-50 start-100 text-black">
                            Press F12 to open the Inspector and see what happens!
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php

        function displaySample($path)
        {
            $contents = file_get_contents($path);

            if (str_ends_with($path, 'php')) {
                require_once($path);
            }
            else {
                echo $contents;
            }

            echo '<br><br>';

            $id = preg_replace('/[^a-z]*/', '', $path);
            echo '<a class="btn btn-warning" data-bs-toggle="collapse" href="#' . $id . '">Display Code</a>';
            echo '<div class="collapse" id="' . $id . '">';
            echo '<pre><code>' . htmlentities($contents) . '</code></pre>';
            echo '</div>';
        }

        function displayCode($path)
        {
            echo '<pre><code>' . htmlentities(file_get_contents($path)) . '</code></pre>';
        }

        ?>

        <section>
            <div class="container">
                <div class="row mb-5 pb-3">
                    <div class="col alert alert-info">
                        <h2>Core Classes</h2>

                        <p>
                            Most of features in <strong>jBob</strong> are implicitly activated adding specific CSS classes directly into the HTML. The <code>init()</code> function provides to attach all the proper jQuery handlers, and manage initialization of other components fetched asynchronously.
                        </p>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.async-modal</h3>

                        <p>
                            When clicked, an <code>.async-modal</code> button will fetch a Bootstrap modal from a given URL and will display it. When closed, the modal is destroyed and removed from the DOM.
                        </p>
                        <p>
                            The URL can be both in the <code>data-modal-url</code> attribute or, if the trigger is an anchor, in the <code>href</code> attribute.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/modal.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.async-tab</h3>

                        <p>
                            A <a href="https://getbootstrap.com/docs/5.3/components/navs-tabs/">Bootstrap tab</a> having the <code>.async-tab</code> CSS class will fetch his contents from the URL in <code>data-tab-url</code> when activated.
                        </p>
                        <p>
                            When the tab is no longer active, his contents are removed again from the DOM and to be reloaded at the next activation. Unless the <code>.keep-contents</code> is also used, in which case the contents are loaded once.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/tab.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.async-accordion</h3>

                        <p>
                            A <a href="https://getbootstrap.com/docs/5.3/components/accordion/">Bootstrap accordion</a> having the <code>.async-accordion</code> CSS class will fetch his contents from the URL in <code>data-accordion-url</code> when activated (every time it is activated, by default; only once, if the <code>.keep-contents</code> is also used).
                        </p>
                        <p>
                            The behavior is to be attached to the main <code>.accordion-item</code>: you can provide your static contents for <code>.accordion-header</code>, the fetched HTML is appended into <code>.accordion-body</code>
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/accordion.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.async-popover</h3>

                        <p>
                            A <code>.async-popover</code> button has a popover which contents as dinamycally fetched from the given URL when activated. On the contrary of other components, an async popover keeps its contents even when closed (so are fetched just once).
                        </p>
                        <p>
                            Remember that <a href="https://getbootstrap.com/docs/5.3/components/popovers/">Bootstrap popovers</a> have to be explicitely inited in your own JS: it is not automatically managed to avoid conflicts with other popovers not handled with <strong>jBob</strong>.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/popover.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.dynamic-form</h3>

                        <p>
                            When submitted a <code>.dynamic-form</code> is serialized (using the <code>serializeForm()</code> function), submitted to the given <code>action</code> URL using his own <code>method</code>, and - on success - a set of actions (configurable, appending specific input:hidden fields to the form itself) are performed.
                        </p>
                        <p>
                            Each relevant input:hidden found in the form is automatically flagged with the <code>.skip-on-submit</code> CSS class, so it is not serialized and added to the submitted payload.
                        </p>
                        <p>
                            Actions are performed on the same order they appear into the form: care to reload the page after you performed other actions!
                        </p>
                        <p>
                            Out of the box actions are:
                        </p>
                        <ul>
                            <li>
                                <code>jb-close-modal</code>: if the form is within a Bootstrap modal, it is closed. This is essentially equivalent to the function provided by <code>.modal-form</code>
                            </li>
                            <li>
                                <code>jb-close-all-modals</code>: all modals on the page are closed, including the one containing the form itself
                            </li>
                            <li>
                                <code>jb-post-saved-function</code>: executes a function defined through the <code>dynamicFunctions</code> attribute of the global jBob configuration
                            </li>
                            <li>
                                <code>jb-reload-page</code>: just reloads the current page
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/dynamicform.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.dynamic-table</h3>

                        <p>
                            A dynamic table permits to add and remove rows dynamically. The click on the element with the CSS class <code>.add-row</code> triggers the append of a new row, the click on <code>.remove-row</code> removes the current row.
                        </p>
                        <p>
                            The contents of <code>tfoot</code> are used as template for newly added contents: remember to hide it, using a specific CSS rule or with a <code>hidden</code> attribute.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/table.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.filterable-panel</h3>

                        <p>
                            A filterable panel includes a <code>&lt;input type="search"&gt;</code> field which triggers a new GET request to the desired endpoint, to perform on the server the required filter and obtain the new filtered contents to display within the area delimited by <code>.filterable-block</code>.
                        </p>
                        <p>
                            Optionally, the <code>search</code> input can be into a more complex form involving other possible attributes to filter. In this case, the whole form is serialized and appended to the request for the new contents.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/filterable.php') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.infinite-scroll</h3>

                        <p>
                            Infinite scroll is based on <a href="https://getbootstrap.com/docs/5.3/components/pagination/">Bootstrap's pagination</a>: you can implement single pages linked each other, using classic pagination, while <strong>jBob</strong> uses the pagination widget as reference to dinamycally load and inject contents in the user's viewport.
                        </p>
                        <p>
                            The <code>.infinite-scroll</code> node's contents of each loaded page will be isolated and appended to the <code>.infinite-scroll</code> in the initial page; the <code>.active</code> class set in the pagination buttons will be used to determine the next page to load.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/scroll1.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.dynamic-pagination</h3>

                        <p>
                            To dynamically load contents paginated with <a href="https://getbootstrap.com/docs/5.3/components/pagination/">Bootstrap's pagination</a>.
                        </p>
                        <p>
                            Adding the <code>.dynamic-pagination</code> class to Bootstrap's <code>.pagination</code>, and delimiting with <code>.pagination-block</code> the area to be updated on page change, the whole HTML page is fetched but only the selected area on screen is replaced.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/pagination1.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.modal-form</h3>

                        <p>
                            A form within a Bootstrap modal and having the <code>.modal-form</code> CSS class, when submitted is serialized (using the <code>serializeForm()</code> function), submitted to the given <code>action</code> URL using his own <code>method</code>, and - on success - the parent modal is closed.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/modalform.html') ?>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="row mb-5 pb-3">
                    <div class="col alert alert-info">
                        <h2>Other Classes</h2>

                        <p>
                            A few other CSS classes are managed, and provide addictional behaviors when found in the DOM.
                        </p>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>.delete-on-close</h3>

                        <p>
                            When applied to a Bootstrap's modal, it is removed from DOM when closed. Natively used for all modals fetched with <code>.async-modal</code>
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displaySample('samples/deleteonclose.html') ?>
                    <div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="row mb-5 pb-3">
                    <div class="col alert alert-info">
                        <h2>JS Methods</h2>

                        <p>
                            <strong>jBob</strong> provides some JS function, mostly used internally and exposed for convenience. Just <code>init()</code> is required.
                        </p>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>init(params)</h3>

                        <p>
                            Accepts an optional object for parameters:
                        </p>
                        <ul>
                            <li>
                                <code>initFunction</code>: a callback to be called on all HTML fragments fetched asynchronously, to provide your own extra initialization on new HTML nodes injected into the page. A parameters is passed to the function, which is the new HTML node itself. This will be called for every new piece of HTML attached in the DOM by <strong>jBob</strong>: when fetching contents with async widgets, when adding a row to a <code>.dynamic-table</code>, when using <code>fetchNode()</code> and more. For your convenience, it is also invoked once on the entire <code>$('body')</code> when the library is inited
                            </li>
                            <li>
                                <code>dynamicFunctions</code>: an object containing all functions intended to be executed within all <code>.dynamic-form</code> (using the <code>jb-post-saved-function</code> ability). Each function may accept two parameters: a jQuery node for the subject form, and the actual payload received as response of his submission.
                            </li>
                            <li>
                                <code>fixBootstrap</code>: an array of <a href="https://getbootstrap.com/docs/5.3/getting-started/javascript/">Bootstrap's jQuery plugins</a> for which enforce initialization. "Modal" and "Popover" are handled by default, you can add others. This may be required under certain situations, where Bootstrap fails to identify jQuery and do not inits his own plugins. In this case, you have to explicitely import Boostrap and make it globally accessible from the <code>window</code> object under the name <code>bootstrap</code>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/init.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>initElements(container)</h3>

                        <p>
                            To be used when you append new elements in the DOM, to call the initialization function on the new fragment. This implies <strong>jBob</strong> own initialization and your own custom, as provided to <code>init()</code>
                        </p>
                        <p>
                            Can be invoked multiple times on the same element (for example: if you append a new <code>.async-tab</code> to an existing tabs group); <strong>jBob</strong> provides to init his own interactions once by tagging the already inited nodes with the <code>.jb-in</code> CSS class.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/initElements.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>assignIDs(selector, container)</h3>

                        <p>
                            For each node matching the jQuery <code>selector</code> found in <code>container</code>, generates a random ID attribute (if none is already found).
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/assignid.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>fetchNode(url, node)</h3>

                        <p>
                            Fetches HTML from the given <code>url</code> and replaces the contents of <code>node</code> in DOM.
                        </p>
                        <p>
                            The function itself returns a <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise">Promise</a> to track actual fetch activity.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/fetchnode.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>reloadNode(node)</h3>

                        <p>
                            Almost like <code>fetchNode()</code>, but can be directly applied to "async" nodes for auto refetch their contents.
                        </p>
                        <p>
                            Otherwise, <code>node</code> must have a <code>data-reload-url</code> attribute with the URL from where get the new HTML. <code>node</code> is replaced with the new HTML (not only his contents).
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/reloadnode.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>submitButton(form)</h3>

                        <p>
                            Utility intended to easily retrieve the submit button of a form; it may be both within the form itself (<code>button[type=submit]</code>) or external (having a form attribute which value matches the id of the intended form).
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/submitbutton.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>serializeForm(form)</h3>

                        <p>
                            Like the jQuery's native <code>serialize()</code>, but skips elements having the <code>.skip-on-submit</code> CSS class.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/serializeform.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>makeSpinner()</h3>

                        <p>
                            Just creates a <code>div</code> with a <a href="https://getbootstrap.com/docs/5.3/components/spinners/">Bootstrap Spinner</a>.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/makeSpinner.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>onScreen(node, offset = 0)</h3>

                        <p>
                            Tests if the given jQuery node is completely within the current viewport. Optionally, pass a second argument with an extra offset to be considered (e.g. if node is at 200px from the viewport).
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/onscreen.html') ?>
                    </div>
                </div>
            </div>
        </section>

        <section>
            <div class="container">
                <div class="row mb-5 pb-3">
                    <div class="col alert alert-info">
                        <h2>JS Events</h2>

                        <p>
                            A few jQuery events are triggered, to permit deeper integration and custom behaviors.
                        </p>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>jb-before-async-fetch</h3>

                        <p>
                            Triggered by <code>.async-modal</code>, <code>.async-tab</code>, <code>.async-accordion</code> and <code>.infinite-scroll</code> before to start the actual fetch of contents.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/jb-before-async-fetch.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>jb-after-async-fetch</h3>

                        <p>
                            Triggered by <code>.async-modal</code>, <code>.async-tab</code>, <code>.async-accordion</code> and <code>.infinite-scroll</code> when the fetch is complete and the retrieved data have been appended in the DOM.
                        </p>
                        <p>
                            The callback receives an extra parameter which is <code>true</code> when everything goes well and <code>false</code> when an error occurred.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/jb-after-async-fetch.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>jb-table-row-added</h3>

                        <p>
                            Triggered by <code>.dynamic-table</code> when a new row is added to the table.
                        </p>
                        <p>
                            The callback receives the row itself as parameter.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/jb-table-row-added.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>jb-table-row-removing</h3>

                        <p>
                            Triggered by <code>.dynamic-table</code> when a row is going to be removed from the table.
                        </p>
                        <p>
                            The callback receives both the table and the row itself as parameter.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/jb-table-row-removing.html') ?>
                    </div>
                </div>

                <div class="row mb-5 pb-3 border-1 border-bottom">
                    <div class="col-12 col-md-6">
                        <h3>jb-table-row-removed</h3>

                        <p>
                            Triggered by <code>.dynamic-table</code> when a row is removed from the table.
                        </p>
                        <p>
                            The callback receives both the table and the row itself as parameter.
                        </p>
                    </div>
                    <div class="col-12 col-md-6">
                        <?php displayCode('samples/jb-table-row-removed.html') ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="use-cases">
            <div class="container">
                <div class="row pb-3">
                    <div class="col alert alert-info">
                        <h2>Use Cases</h2>

                        <p>
                            <strong>jBob</strong> has been implemented by an old-school developer who never embraced the paradigm proposed by frameworks like React or Angular (tragically foundered on "modern" SSR techniques, that is: generating the HTML on the server, in the same way as Tim Berners-Lee did in 1990, but with far more complex tooling) but who did not want to give up building web applications with dynamic interactions.
                        </p>
                        <p>
                            Here are a few suggestions on how to leverage <strong>jBob</strong> in your applications.
                        </p>
                    </div>
                </div>

                <div class="row" data-masonry='{"percentPosition": true }'>
                    <div class="col-12 col-md-4">
                        <div class="card p-3 mb-4">
                            <p>
                                Use <code>.async-modal</code> for the "delete" buttons in your interface, so to generate on-demand a notice modal with informations about the element to be deleted, ask confirmation, and provide the actual "delete" code.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="card p-3 mb-4">
                            <p>
                                Use <code>.async-tab</code> for more complex panels, so to posticipate the generation of all involved sub-panels and provide a faster experience.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="card p-3 mb-4">
                            <p>
                                The <code>.dynamic-table</code> can actually be used for any kind of multiple, grouped, dynamic values to be recorded, such as users' contacts (multiple emails, multiple phone numbers...) or filtering (multiple rules described by multiple parameters).
                            </p>
                            <p>
                                Handle all the names of inputs within the table as arrays to permit full serialization of the contents.
                            </p>
                            <br>
                            <?php displayCode('samples/usecase-dynamictable.html') ?>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="card p-3 mb-4">
                            <p>
                                The <code>.skip-on-submit</code> class can be used client-side to decorate a form with addictional informations, to be retrieved and managed before or after the actual submission (as in <code>.dynamic-form</code>). Implement your business logic once, and trigger it according to the fields in each form.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="card p-3 mb-4">
                            <p>
                                <code>fetchNode()</code> can be used to populate a portion of your page according to the selection of a select or a radio button, where each option provides (perhaps with a data attribute?) his own URL to be fetched.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="card p-3 mb-4">
                            <p>
                                The initial purpose of <code>.async-popover</code> was to provide contextual informations about elements, without having to retrieve them all the times the elements themselves are shown. Get them only when required to offload pages' generation.
                            </p>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="card p-3 mb-4">
                            <p>
                                <code>.infinite-scroll</code> provides a naive solution that plays well with the pagination features of most web frameworks: you just have to generate the different pages, easily discoverable by crawlers (for SEO purpose), and <strong>jBob</strong> provides to concatenate them to obtain an infinite scroll effect.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="contacts-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h3>JBOB</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col links">
                        <p>
                            <i class="bi bi-envelope me-2"></i>
                            <a href="mailto:info@madbob.org">info@madbob.org</a>
                        </p>
                        <p>
                            <i class="bi bi-gitlab me-2"></i>
                            <a href="https://gitlab.com/madbob/jbob">gitlab.com/madbob/jbob</a>
                        </p>
                        <p>
                            <i class="bi bi-box me-2"></i>
                            <a href="https://www.npmjs.com/package/jbob">npmjs.com/package/jbob</a>
                        </p>
                        <p>
                            <i class="bi bi-gear me-2"></i>
                            powered by <a href="https://madbob.org/"><img src="images/mad.png" alt="Mad" title="Mad"></a>
                        </p>
                    </div>
                    <div class="col">
                        <a href="https://www.paypal.com/donate?hosted_button_id=GRKVVL3E5KZ4Q">
                            <img src="/images/paypal.png" alt="Paypal" border="0">
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <!-- Matomo -->
    <script type="text/javascript">
      var _paq = window._paq = window._paq || [];
      _paq.push(["setDoNotTrack", true]);
      _paq.push(["disableCookies"]);
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
        var u="//stats.madbob.org/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '29']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
      })();
    </script>
    <noscript><p><img src="//stats.madbob.org/matomo.php?idsite=29&amp;rec=1" style="border:0;" alt="" /></p></noscript>
    <!-- End Matomo Code -->
</body>

</html>
